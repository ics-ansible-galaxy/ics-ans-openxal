import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("openxal")


def test_openxal_installed(host):
    assert host.file("/opt/OpenXAL/lib/library.jar").exists
    assert host.file("/opt/OpenXAL/optics/current/main.xal").exists
    assert host.file("/etc/openxal/xal.smf.data.prefs").exists
